package service;

import java.time.LocalDate;

import db.DriversRepository;
import db.IRepository;
import db.ModelsRepository;
import db.entities.DriverEntity;
import db.entities.ModelEntity;
import ui.DriversState;
import ui.ModelState;

public class RentCompanyService {
	IRepository<DriverEntity, Integer> driverRepository = new DriversRepository();
	IRepository<ModelEntity, String> modelRepository = new ModelsRepository();

	public DriversState addDriver(int licenseId, String name, LocalDate birthDate, String phone) {
		if (driverRepository.existsById(licenseId))
			return DriversState.ALL_READY_EXISTS;
		DriverEntity driverEntity = new DriverEntity(licenseId, name, birthDate, phone);
		driverEntity = driverRepository.save(driverEntity);
		if (driverEntity == null)
			return DriversState.ERROR;
		return DriversState.OK;
	}

	public ModelState addModel(String modelName, int gasTank, String company, String country, int priceDay) {
		if (modelRepository.existsById(modelName))
			return ModelState.ALL_READY_EXISTS;
		ModelEntity modelEntity = new ModelEntity(modelName, gasTank, company, country, priceDay);
		modelEntity = modelRepository.save(modelEntity);
		if (modelEntity == null)
			return ModelState.ERROR;
		return ModelState.OK;
	}
	
//	public DriverDto getDriverById(int id) {
//		// driverEntity -> DriverDto
//	}
}
