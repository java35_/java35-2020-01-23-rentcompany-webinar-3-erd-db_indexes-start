package db.embedded;

import db.entities.CarEntity;

public interface IRentCompanyEmbedded {
	CarEntity saveCar(CarEntity entity);
}
