package db;

import java.util.List;

import db.entities.ModelEntity;

public class ModelsRepository implements IRepository<ModelEntity, String>{
	@Override
	public ModelEntity findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ModelEntity save(ModelEntity entity) {
		if (entity == null)
			throw new IllegalArgumentException();
		return rentCompany.saveModel(entity);
	}

	@Override
	public List<ModelEntity> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean existsById(String id) {
		if (id == null || id.isBlank())
			throw new IllegalArgumentException();
		return rentCompany.existsByModelId(id);
	}

}
